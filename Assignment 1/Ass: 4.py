Var_1 = True
Var_2 = False
print("For AND operation")  # For And operation
print("False & True gives: ", Var_2 and Var_1)
print("False & False gives: ", Var_2 and Var_2)
print("True & True gives: ", Var_1 and Var_1)
print("True & False gives: ", Var_1 and Var_2)
print("For OR operation")  # For Or operation
print("False & True gives: ", Var_2 or Var_1)
print("False & False gives: ", Var_2 or Var_2)
print("True & True gives: ", Var_1 or Var_1)
print("True & False gives: ", Var_1 or Var_2)
print("For NOT operation")  # For Not operation
print("True :", not Var_1)
print("False:", not Var_2)
