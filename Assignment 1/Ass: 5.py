print(bin(5))
print(bin(5 >> 1))
print(5 >> 1)
print(bin(12 << 1))
print(12 << 1)
print(6 << 2)
print('''Conclusion:-

Right shift divides the number we shift in half as many times we shift.
Left shift multiplies the number by 2 as many times we shift.''')
