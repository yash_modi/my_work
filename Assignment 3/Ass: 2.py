# "A" part of 2 question

l1 = [x for x in range(1, 11)]
print(l1)

# "B" part of 2 question

l2 = [x for x in range(10, 110, 10)]
print(l2)

# "C" part of 2 question

l3 = ["python", "django", "flash", "string", "function", "classes"]
print(l3)

# "D" part of 2 question

l4 = {"l1": l1, "l2": l2, "l3": l3}
print(l4)

# "E" part of 2 question

main_list = l1 + l2 + l3
print(main_list)

# "F" part of 2 question

l5 = l1 * 2
print(l5)

# "G" part of 2 question

main_list.append(l5)
print(main_list)

# "H" part of 2 question

print(main_list.count(1))
